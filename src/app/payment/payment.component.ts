
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

declare var Razorpay: any;

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  title = 'Fluffy Tails';
  paymentId: string="";
  error: string="";
  form: any = {};
  total:any;
  order_id:any;

  customer:any;



  constructor(private http: HttpClient,private route:ActivatedRoute){
   
  }


  ngOnInit() {
    
    this.total = this.route.snapshot.paramMap.get('total');
    
    }
    payNow() {
      this.customer = localStorage.getItem("user");
      
      const RozarpayOptions = {
        description: 'Sample Razorpay demo',
        currency: 'INR',
        amount: this.total,
        name: 'Fluffy Tails',
        key: 'rzp_test_nWZxyq10ewkHpw',
        image: '',
        prefill: {
          name: "this.customer.name",
          email: "this.customer.email" ,
          phone: "this.customer.phoneNumber" 
        },
        theme: {
          color: '#d6b9b0'
        },
        modal: {
          ondismiss:  () => {
            console.log('dismissed')
          }
        }
      }
  
      const successCallback = (paymentid: any) => {
        console.log(paymentid);
      }
  
      const failureCallback = (e: any) => {
        console.log(e);
      }
  
      Razorpay.open(RozarpayOptions,successCallback, failureCallback);
    }


}



// import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
// import { Router } from '@angular/router';
// import { PaymentService } from '../payment.service';

// @Component({
//   selector: 'app-payment',
//   templateUrl: './payment.component.html',
//   styleUrls: ['./payment.component.css']
// })
// export class PaymentComponent implements OnInit {

//   amount = 0;

//   @ViewChild('paymentRef', {static: true}) paymentRef!: ElementRef;

//   constructor(private router: Router, private payment: PaymentService,private ngZone: NgZone) { }
   
//   ngOnInit(): void {
//     this.amount = this.payment.totalAmount;
//     console.log(window.paypal);
//     window.paypal.Buttons(
//       {
//         style: {
//           layout: 'horizontal',
//           color: 'blue',
//           shape: 'rect',
//           label: 'paypal',
//         },
 
//         createOrder: (data: any, actions: any) => {
//           return actions.order.create({
//             purchase_units: [
//               {
//                 amount: {
//                   value: this.amount.toString(),
//                   currency_code: 'USD'
//                 }
//               }
//             ]
//           });
//         },   
//         onApprove: (data: any, actions: any) => {
//           return actions.order.capture().then((details: any) => {
//             // if (details.status === 'COMPLETED') {
//             //   this.payment.transactionID = details.id;
//             //   this.router.navigate(['confirm']);  
//             // }
//             this.payment.transactionID = details.id;
//           //  this.click();
//           console.log(details);

//           this.ngZone.run(() => {
//             this.router.navigate(['/confirm']); // Navigate to the customer page
//           });
//             // this.router.navigate(['confirm']);  
            
//           }).catch((error: any) => {
//             console.log('Error during payment capture:', error);
//           });
//         },
//         onError: (error: any) => {
//           console.log(error);
//         }
//       }
//     ).render(this.paymentRef.nativeElement);
//   }

//   cancel() {
//     this.router.navigate(['cart']);
//   }


//   click(){
//     this.router.navigate(['con']);
//   }


// }